var myFullpage = new fullpage('#fullpage', {
    menu: '#scroll-menu',
    lockAnchors: false,
    anchors:['section-1', 'section-2', 'section-3', 'section-4','section-5'],

    css3: true,
    scrollingSpeed: 700,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    fadingEffect: true,
    touchSensitivity: 15,
    keyboardScrolling: true,
    animateAnchor: true,
    recordHistory: true,
    lazyLoading: true,
});

jQuery(document).ready(function ($) {

    $('#to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

    // Header link smooth scroll
    $(".anchor").on("click", "a", function (event) {
        anchorSmooth(event, $(this));
    });

    // Anchor Smooth scroll
    function anchorSmooth(event, el) {
        //href anchor
        let str = /#(.+)/.exec(el.attr('href'))[0];

        //якщо блок інсує, зробити перехід , або редірект на сторінку з блоком
        if ($(str).length > 0) {
            event.preventDefault(el);
            let top = $(str).offset().top; //узнаем высоту от начала страницы до блока на который ссылается якорь
            $('body,html').animate({scrollTop: top}, 1500);
        }

    }

    $('.hamburger-clickable').click(function () {
        $('.hamburger-clickable').toggleClass('is-active');
        $('#general-menu').fadeToggle('fast');
        $('body').toggleClass('popup-active');
    });

    $('.hamburger-close').click(function () {
        let target = $(this).data("target");
        $(target).fadeToggle('fast');
        $('body').toggleClass('popup-active');
    });

    $('.popup-open a, a.popup-open').click(function() {
        let target = $(this).attr("href");
        $(target).fadeToggle('fast');
        $('body').addClass('popup-active');

        if(document.getElementById('general-menu').style.display === 'block') {
            $('.hamburger-clickable').toggleClass('is-active');
        }
        $('.modal').not(target).fadeOut();
    });

    $('#footer-toggle').click(function() {
        $('#footer-menu').toggleClass('active');
    });

});