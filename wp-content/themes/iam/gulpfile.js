"use strict"

// Load plugins
const gulp = require('gulp');
const sass = require('gulp-sass');
const minify = require('gulp-minify');
const browsersync = require('browser-sync').create();
const debug = require('gulp-debug');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');

require('dotenv').config();
const proxy_url = process.env.BROWSERSYNC_PROXY_URL || 'http://iam.rrr:8888/';

// BrowserSync
function browserSync(done) {
    browsersync.init({
        injectChanges: true,
        proxy: proxy_url,
        host: proxy_url,
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// CSS task
function css() {
    return gulp.src('style.scss')
    .pipe(plumber({
            errorHandler: notify.onError(function (err) {
                return{
                    title: "Error",
                    message: err.message
                }
            })
    }))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(autoprefixer({
        // grid: 'autoplace',
        // browsers: ['last 4 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('.'))
    .pipe(browsersync.stream());
}

// Watch files
function watchFiles() {
    gulp.watch("style/**/*.scss", css);
    gulp.watch("*.scss", css);
    gulp.watch('*.*', browserSyncReload);
}

const watch = gulp.parallel(watchFiles, browserSync);

exports.css = css;
exports.default = watch;
