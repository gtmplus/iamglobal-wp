<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

</main>

<footer id="footer" class="footer-section">
    <div id="footer-toggle"></div>
    <div id="footer-menu" class="footer-menu flex-between">
        <nav class="footer-menu__wrap">
            <?php $footer_menu = wp_get_nav_menu_items(4);
            if(!empty($footer_menu)) :
                foreach($footer_menu as $item) : ?>
                    <a href="<?php echo $item->url; ?>" class="popup-open" data-id="<?php echo $item->object_id; ?>"><?php echo $item->title; ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </nav>
        <div class="footer-lang">
            <a href="#">DE</a> | <a href="#">EN</a>
        </div>
    </div>
</footer>

<div id="general-menu" class="modal general-menu">
    <div class="general-menu__top">
        <div class="hamburger hamburger-clickable hamburger--squeeze">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>

        <div class="general-menu__top-menu">
            <?php $general_menu = wp_get_nav_menu_items(2);
            if(!empty($general_menu)) :
                foreach($general_menu as $item) : ?>
                    <a href="<?php echo $item->url; ?>" class="popup-open" data-id="<?php echo $item->object_id; ?>"><?php echo $item->title; ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="language-switcher">
                <a href="#">Deutsch</a> | <a href="#">English</a>
            </div>
        </div>
    </div>
    <div class="general-menu__bottom">
        <div class="general-menu__bottom-menu">
            <?php $general_sub_menu = wp_get_nav_menu_items(3);
            if(!empty($general_sub_menu)) :
                foreach($general_sub_menu as $item) : ?>
                    <a href="<?php echo $item->url; ?>" class="popup-open" data-id="<?php echo $item->object_id; ?>"><?php echo $item->title; ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div id="data-protection" class="modal modal-page">
    <div class="hamburger hamburger-close hamburger--squeeze is-active" data-target="#data-protection">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div class="modal-page__heading">
        <h2>Datenschutzerklärung</h2>
        <div class="hr"></div>
    </div>
    <div class="modal-page__body">
        <p><b>Bei weitergehenden Fragen zum Datenschutz kontaktieren Sie bitte Herrn Günther Kriele: g.kriele@iam-global.de</b></p>
        <p><b>Hinweise zur Datenverarbeitung im Zusammenhang mit Google Analytics</b></p>
        <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Ireland Limited. Wenn der Verantwortlicher für die Datenverarbeitung auf dieser Website außerhalb des Europäischen Wirtschaftsraumes oder der Schweiz sitzt, dann erfolgt die Google Analytics Datenverarbeitung durch Google LLC. Google LLC und Google Ireland Limited werden nachfolgend „Google“ genannt.</p>
        <p>Google Analytics verwendet sog. „Cookies“, Textdateien, die auf dem Computer des Seitenbesuchers gespeichert werden und die eine Analyse der Benutzung der Website durch den Seitenbesucher ermöglichen. Die durch das Cookie erzeugten Informationen über die Benutzung dieser Website durch den Seitenbesucher (einschließlich der gekürzten IP-Adresse) werden in der Regel an einen Server von Google übertragen und dort gespeichert.</p>
        <p>Google Analytics wird ausschließlich mit der Erweiterung „_anonymizeIp()“ auf dieser Website verwendet. Diese Erweiterung stellt eine Anonymisierung der IP-Adresse durch Kürzung sicher und schließt eine direkte Personenbeziehbarkeit aus. Durch die Erweiterung wird die IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Die im Rahmen von Google Analytics von dem entsprechenden Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>
        <p>Im Auftrag des Seitenbetreibers wird Google die anfallenden Informationen benutzen, um die Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen dem Seitenbetreiber gegenüber zu erbringen (Art. 6 Abs. 1 lit. f DSGVO). Das berechtigte Interesse an der Datenverarbeitung liegt in der Optimierung dieser Website, der Analyse der Benutzung der Website und der Anpassung der Inhalte. Die Interessen der Nutzer werden durch die Pseudonymisierung hinreichend gewahrt.</p>
        <p>Google LLC. ist nach dem sog. Privacy Shield zertifiziert (Listeneintrag hier) und gewährleistet auf dieser Basis ein angemessenes Datenschutzniveau. Die gesendeten und mit Cookies, Nutzerkennungen (z. B. User-ID) oder Werbe-IDs verknüpften Daten werden nach 50 Monaten automatisch gelöscht. Die Löschung von Daten, deren Aufbewahrungsdauer erreicht ist, erfolgt automatisch einmal im Monat.</p>
        <p>Die Erfassung durch Google Analytics kann verhindert werden, indem der Seitenbesucher die Cookie-Einstellungen für diese Website anpasst. Der Erfassung und Speicherung der IP-Adresse und der durch Cookies erzeugten Daten kann außerdem jederzeit mit Wirkung für die Zukunft widersprochen werden. Das entsprechende Browser- Plugin kann unter dem folgenden Link heruntergeladen und installiert werden: https://tools.google.com/dlpage/gaoptout.</p>
        <p>Der Seitenbesucher kann die Erfassung durch Google Analytics auf dieser Webseite verhindern, indem er auf folgenden Link klickt. Es wird ein Opt-Out-Cookie gesetzt, der die zukünftige Erfassung der Daten beim Besuch dieser Website verhindert.</p>
        <p>Weitere Informationen zur Datennutzung durch Google, Einstellungs- und Widerspruchsmöglichkeiten, finden sich in der Datenschutzerklärung von Google (https://policies.google.com/privacy) sowie in den Einstellungen für die Darstellung von Werbeeinblendungen durch Google (https://adssettings.google.com/authenticated).</p>
        <p>reCAPTCHA</p>
        <p>Zum Schutz Ihrer Anfragen per Internetformular verwenden wir den Dienst reCAPTCHA des Unternehmens Google Inc. (Google). Die Abfrage dient der Unterscheidung, ob die Eingabe durch einen Menschen oder missbräuchlich durch automatisierte, maschinelle Verarbeitung erfolgt. Die Abfrage schließt den Versand der IP-Adresse und ggf. weiterer von Google für den Dienst reCAPTCHA benötigter Daten an Google ein. Zu diesem Zweck wird Ihre Eingabe an Google übermittelt und dort weiter verwendet. Ihre IP-Adresse wird von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung dieses Dienstes auszuwerten. Die im Rahmen von reCaptcha von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Für diese Daten gelten die abweichenden Datenschutzbestimmungen des Unternehmens Google. Weitere Informationen zu den Datenschutzrichtlinien von Google finden Sie unter: https://policies.google.com/privacy?hl=de</p>
    </div>
</div>

<div id="impressum" class="modal modal-page">
    <div class="hamburger hamburger-close hamburger--squeeze is-active" data-target="#impressum">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div class="modal-page__heading">
        <h2>Impressum</h2>
        <div class="hr"></div>
    </div>
    <div class="modal-page__body">
        <p><b>Allgemeine Informationen:</b></p>
        <p><b>Name des Unternehmens</b><br>IAM Global GmbH</p>
        <p><b>Rechtsform</b><br>Gesellschaft mit beschränkter Haftung (GmbH)</p>
        <p><b>Anschrift</b><br>Chlodwigstraße 90, 40225 Düsseldorf</p>
        <p><b>Niederlassung</b><br>Merowingerplatz 1, 40225 Düsseldorf</p>
        <p><b>Geschäftsführer</b><br>Günther Kriele, Christian Balloff</p>
        <p><b>Handelsregister</b><br>Amtsgericht Düsseldorf - HRB 83800</p>
        <p><b>Umsatzsteuer-Identifikationsnummer</b><br>DE 318 934 253</p>
        <p><b>Sitz der Gesellschaft</b><br>Düsseldorf</p>
        <p><b>Telefon</b><br>+49 211 97266300</p>
        <p><b>E-Mail</b><br>info@iam-global.de</p>
        <p><b>Geschäftsführer</b><br>Günther Kriele, Christian Balloff</p>
        <p><b>Handelsregister</b><br>Amtsgericht Düsseldorf - HRB 83800</p>
        <h4>Downloads</h4>
        <p>Allgemeine Geschäftsbedingungen der IAM Global GmbH<br>AGB_IAM_Global_GmbH_06_2018.pdf<br>Adobe Acrobat Dokument 282.9 KB</p>
        <a href="#" class="btn" download><?php echo __('Download', 'iam'); ?></a>
        <p>Terms & Conditions of IAM Global GmbH<br>T&C_IAM_Global_GmbH_06_2018.pdf<br>Adobe Acrobat Dokument 350.0 KB</p>
        <a href="#" class="btn" download><?php echo __('Download', 'iam'); ?></a>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>