<?php

if ( ! function_exists( 'iam_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 */
	function iam_setup() {

		/*
		 * Make theme available for translation.
		 */
		load_theme_textdomain( 'iam', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 825, 510, true );


		/*
		 * Switch default core markup for search form, 'comment-form', 'comment-list',
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'gallery',
			'caption'
		) );

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'status',
			'audio'
		) );

		/*
		 * Enable support for custom logo.
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 248,
			'width'       => 248,
			'flex-height' => true,
		) );


		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'yoast-seo-breadcrumbs' );

		$upload_dir = wp_upload_dir();

		define( 'THEME_URI', get_template_directory_uri() );

		define( 'THEME_DIR', get_template_directory() );
		define('UPLOADS_URL', trailingslashit($upload_dir['baseurl']));
	}
endif; // iam_setup

add_action( 'after_setup_theme', 'iam_setup', 3 );



function includes() {

    /**
     * Customizer additions.
     *
     */
    require THEME_DIR . '/inc/template-functions.php';

	/**
	 * Customizer additions.
	 *
	 */
	require THEME_DIR . '/inc/customizer.php';

	/**
	 * Site functions optimization
	 */
	require THEME_DIR . '/inc/function-optimization.php';

	/**
	 * Hooks for ajax
	 */
	require THEME_DIR . '/inc/hooks.php';

	/**
	 * Register thumbnails
	 */
	require THEME_DIR . '/inc/thumbnails.php';
}

// Load the theme includes.
add_action( 'after_setup_theme',  'includes', 4 );

$theme = wp_get_theme();
define( 'THEME_VERSION', $theme->Version ); //gets version written in your style.css

/**
 * Register widget area.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function iam_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'iam' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'iam' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Widget footer 1',
		'id'            => 'custom-header-widget1',
		'before_widget' => '<div class="chw-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="chw-title">',
		'after_title'   => '</p>',
	) );

	register_sidebar( array(
		'name'          => 'Widget footer 2',
		'id'            => 'custom-header-widget2',
		'before_widget' => '<div class="chw-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="chw-title">',
		'after_title'   => '</p>',
	) );

	register_sidebar( array(
		'name'          => 'Widget footer 3',
		'id'            => 'custom-header-widget3',
		'before_widget' => '<div class="chw-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="chw-title">',
		'after_title'   => '</p>',
	) );

	register_sidebar( array(
		'name'          => 'Widget footer 4',
		'id'            => 'custom-header-widget4',
		'before_widget' => '<div class="chw-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="chw-title">',
		'after_title'   => '</p>',
	) );

	register_sidebar( array(
		'name'          => 'Widget footer 5',
		'id'            => 'custom-header-widget5',
		'before_widget' => '<div class="chw-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="chw-title">',
		'after_title'   => '</p>',
	) );
}

add_action( 'widgets_init', 'iam_widgets_init' );


/**
 * Add preconnect for Google Fonts.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed.
 *
 * @return array URLs to print for resource hints.
 */
function iam_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'iam-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '>=' ) ) {
			$urls[] = array(
				'href' => 'https://fonts.gstatic.com',
				'crossorigin',
			);
		} else {
			$urls[] = 'https://fonts.gstatic.com';
		}
	}

	return $urls;
}

//add_filter('wp_resource_hints', 'iam_resource_hints', 10, 2);


/**
 * Display descriptions in main navigation.
 *
 * @param string $item_output The menu item output.
 * @param WP_Post $item Menu item object.
 * @param int $depth Depth of the menu.
 * @param array $args wp_nav_menu() arguments.
 *
 * @return string Menu item with possible description.
 */
function iam_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}

add_filter( 'walker_nav_menu_start_el', 'iam_nav_description', 10, 4 );


/**
 * custom code
 */

// wp_nav_menu().
register_nav_menus( array(
	'general' => __( 'General Menu', 'iam' ),
	'general_sub' => __( 'General Sub Menu', 'iam' ),
	'right_content' => __( 'Right Menu', 'iam' ),
    'footer'     => __( 'Footer Menu', 'iam' ),
) );

/**
 * Custom Scripts in footer
 */
add_action( 'wp_enqueue_scripts', 'scripts_init' );
function scripts_init() {
    wp_enqueue_style( 'fullpage', THEME_URI . '/assets/css/fullpage.min.css');
    wp_enqueue_style( 'style', THEME_URI . '/style.css');
	wp_enqueue_script( 'easing-js', THEME_URI . '/js/easings.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'scrolloverflow-js', THEME_URI . '/js/scrolloverflow.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'fullpage-js', THEME_URI . '/js/fullpage.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'main-js', THEME_URI . '/js/main.js', array( 'jquery' ), null, true );
}

/**
 * Enqueue scripts for ajax
 */
function myajax_data() {
	wp_localize_script( 'main-js', 'rivo_ajax',
		array(
			'ajaxurl'  => admin_url( 'admin-ajax.php' ),
			'noposts'  => esc_html__( 'That\'s all for now', 'iam' ),
			'loadmore' => esc_html__( 'Load more', 'iam' ),
			'moreworks' => esc_html__( 'More works', 'iam' ),
		)
	);
}

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );