<?php
/**
 * Blog template file
 *
 */

get_header(); ?>

    <div class="wrap">
        <div class="blog-wrap aside-wrap">


	                <?php if ( have_posts() ) : ?>
                        <div class="page-header">
                            <div class="content">
			                <?php
			                the_archive_title( '<h1 class="page-title">', '</h1>' );
			                the_archive_description( '<div class="taxonomy-description">', '</div>' );
			                ?>
                            </div>
                        </div><!-- .page-header -->
	                <?php endif; ?>

            <?php if (get_post_type() == 'post'):?>
                    <aside class="aside-section">
                        <div class="category-wrap">

			                <?php
			                $args = array(
				                'show_option_all'     => 'All',
				                'show_option_none'    => null,
				                'orderby'             => 'name',
				                'order'               => 'ASC',
				                'style'               => 'list',
				                'show_count'          => 0,
				                'hide_empty'          => 1,
				                'use_desc_for_title'  => 1,
				                'child_of'            => 0,
				                'feed'                => '',
				                'feed_type'           => '',
				                'feed_image'          => '',
				                'exclude'             => 1,
				                'exclude_tree'        => '',
				                'include'             => '',
				                'hierarchical'        => true,
				                'title_li'            => null,
				                'number'              => null,
				                'echo'                => 0,
				                'depth'               => 0,
				                'current_category'    => 0,
				                'pad_counts'          => 0,
				                'taxonomy'            => 'category',
				                'walker'              => 'Walker_Category',
				                'hide_title_if_empty' => false,
				                'separator'           => '<br />',
			                );

			                $category = wp_list_categories( $args );

			                echo '<ul>' . $category . '</ul>'; ?>
                        </div>
                    </aside>
<?php endif; ?>

                    <div class="articles-section">

						<?php
						if ( have_posts() ) :
							/* Start the Loop */
							while ( have_posts() ) : the_post();
								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/post/content', 'excerpt' );
							endwhile;
							?>
                            <div class="content pagination">
								<?php
								the_posts_pagination( array(
									'prev_text'          => '<span class="">' . __( '<<', 'iam' ) . '</span>',
									'next_text'          => '<span class="">' . __( '>>', 'iam' ) . '</span>',
									'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
								) );
								?>

								<?php if ( get_the_posts_pagination() ): ?>
                                    <a href="#" id="more_posts"
                                       class="btn-clear--more"><span
                                            class="btn-text"><?php echo __( 'Load more', 'rovoagency' ); ?></span><i></i></a>
								<?php endif; ?>
                            </div>

						<?php
						else :

							get_template_part( 'template-parts/post/content', 'none' );

						endif;
						?>
                    </div>
        </div>
    </div><!-- .wrap -->

<?php get_footer();
