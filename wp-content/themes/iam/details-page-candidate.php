<?php
/**
 * Template name: Details page (Candidate list)
 */

get_header();
global $post;
$post_type = get_field('post_type');
$category_left = get_field($post_type . '_category_left');
$category_right = get_field($post_type . '_category_right');
$parent_section = intval(get_field('parent_section'));

?>
    <section class="section first-section details-section" style="background-image: url(<?php echo get_field('background'); ?>);">
        <div class="section-wrapper">
            <div class="section-content">
                <div class="screen-overlay"></div>
                <div class="screen-inner">
                    <h2><?php echo get_field('title'); ?><span><?php echo get_field('subtitle'); ?></span></h2>
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-details-content">
        <div class="content-wrap">
            <?php the_content(); ?>
        </div>
        <div class="content-cpt-list">
            <div class="content-cpt-column">
                <h4><?php echo $category_left->name; ?></h4>
                <div class="hr"></div>
                <div class="content-cpt-items">
                    <?php get_candidate_list($post_type, $category_left->taxonomy, $category_left->term_id); ?>
                </div>
            </div>
            <div class="content-cpt-column">
                <h4><?php echo $category_right->name; ?></h4>
                <div class="hr"></div>
                <div class="candidate-list">
                    <?php get_candidate_list($post_type, $category_right->taxonomy, $category_right->term_id); ?>
                </div>
            </div>
        </div>
    </section>

    <ul id="scroll-menu" class="section-menu">
        <?php $general_menu = wp_get_nav_menu_items(5);
        if(!empty($general_menu)) : $i = 1;
            foreach($general_menu as $item) : ?>
                <li data-menuanchor="section-<?php echo $i; ?>" <?php echo $i === $parent_section ? 'class="active"' : ''; ?>><a href="<?php echo get_permalink( $post->post_parent ) . $item->url; ?>"><?php echo $item->title; ?></a></li>
            <?php $i++; endforeach; ?>
        <?php endif; ?>
    </ul>

<?php get_footer(); ?>