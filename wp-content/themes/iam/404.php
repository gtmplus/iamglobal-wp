<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header('', array( 'color' => 'dark' ) ); ?>

<div class="wrap">
	<div class="content-240">

			<section class="error-404 not-found">
				<div class="page-header">
                    <div title="404"><h1 title="404" class="page-title glitch">404</h1></div>
					<h2 class="text-center"><?php _e( 'Oops! That page can&rsquo;t be found.', 'iam' ); ?></h2>
				</div><!-- .page-header -->
<!--				<div class="page-content">-->
<!--					<p>--><?php //_e( 'It looks like nothing was found at this location. Maybe try a search?', 'iam' ); ?><!--</p>-->
<!---->
<!--					--><?php //get_search_form(); ?>
<!---->
<!--				</div>-->
			</section><!-- .error-404 -->

    </div>
</div><!-- .wrap -->

<?php get_footer();
