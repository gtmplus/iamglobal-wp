<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

    <div class="wrap">
        <div class="content-less">


        <?php if (has_post_thumbnail() ):
            $attr = array(
            'class' => 'page-image',
        );

             $img = wp_get_attachment_image(get_post_thumbnail_id( $post->ID ), 'full', false, $attr);
                echo $img;
         endif; ?>
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">

                    <?php
                    while (have_posts()) : the_post();

                        get_template_part('template-parts/page/content', 'page');

                    endwhile; // End of the loop.
                    ?>

                </main><!-- #main -->
            </div><!-- #primary -->
        </div>
    </div><!-- .wrap -->

<?php get_footer();