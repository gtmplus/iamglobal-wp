<?php
/**
 * Template name: Sections page
 */

get_header();

?>

<div id="fullpage">
    <?php
        if( have_rows('sections') ): $i = 1;

            while( have_rows('sections') ) : the_row(); ?>

                <section data-anchor="section-<?php echo $i; ?>" class="section first-section scroll-section" style="background-image: url(<?php echo get_sub_field('background'); ?>);">
                    <div class="section-wrapper">
                        <div class="section-content">
                            <div class="screen-overlay"></div>
                            <div class="screen-inner">
                                <h2><?php echo get_sub_field('title'); ?><span><?php echo get_sub_field('subtitle'); ?></span></h2>
                                <div class="hr"></div>
                                <div class="section-description">
                                    <?php echo get_sub_field('description'); ?>
                                </div>
                                <a href="<?php echo get_sub_field('button_link'); ?>" class="btn"><?php echo get_sub_field('button_text'); ?></a>
                            </div>
                        </div>
                    </div>
                </section>

            <?php $i++; endwhile;

        else :
            echo "Content is missing";
        endif;
    ?>
</div>

<ul id="scroll-menu" class="section-menu">
    <?php $general_menu = wp_get_nav_menu_items(5);
    if(!empty($general_menu)) : $i = 1;
        foreach($general_menu as $item) : ?>
            <li data-menuanchor="section-<?php echo $i; ?>" <?php echo $i === 1 ? 'class="active"' : ''; ?>><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
        <?php $i++; endforeach; ?>
    <?php endif; ?>
</ul>

<?php get_footer(); ?>