<?php
$image = !empty(get_the_post_thumbnail_url(null, 'blog-thumb')) ? get_the_post_thumbnail_url(null, 'blog-thumb') : get_template_directory_uri() . '/img/blog-placeholder.jpg';

?>

<div class="blog-item">
    <div class="blog-item__left">
        <img src="<?php echo $image; ?>" alt="blog-img">
    </div>
    <div class="blog-item__right">
        <div class="blog-item__right-content">
            <h5><?php echo get_the_title(); ?></h5>
            <?php echo get_the_excerpt(); ?>
        </div>
        <a href="<?php echo get_the_permalink(); ?>" class="btn">Weiterlesen</a>
    </div>
</div>
