<?php
$image = !empty(get_the_post_thumbnail_url(null, 'candidate-thumb')) ? get_the_post_thumbnail_url(null, 'candidate-thumb') : get_template_directory_uri() . '/img/placeholder.jpg';
$full_image = !empty(get_the_post_thumbnail_url(null, 'candidate-full')) ? get_the_post_thumbnail_url(null, 'candidate-full') : get_template_directory_uri() . '/img/candidate-full.jpg';

?>

<div class="candidate-item">
    <div class="candidate-item__left">
        <img src="<?php echo $image; ?>" alt="avatar">
    </div>
    <div class="candidate-item__right">
        <div class="candidate-item__right-content">
            <h5><?php echo get_the_title(); ?></h5>
            <?php echo get_the_excerpt(); ?>
        </div>
        <a href="#candidate-<?php echo get_the_ID(); ?>" class="popup-open btn">Weiterlesen</a>
    </div>
    <div id="candidate-<?php echo get_the_ID(); ?>" class="modal modal-page candidate-modal">
        <div class="hamburger hamburger-close hamburger--squeeze is-active" data-target="#candidate-<?php echo get_the_ID(); ?>">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="modal-page__heading">
            <h2><?php echo get_the_title(); ?></h2>
            <div class="hr"></div>
        </div>
        <div class="modal-page__body">
            <div class="candidate-content">
                <div class="candidate-content__left">
                    <img src="<?php echo $full_image; ?>" alt="candidate_full">
                </div>
                <div class="candidate-content__right">
                    <h5>Rezension</h5>
                    <div>
                        <?php echo get_the_content(); ?>
                    </div>
                </div>
            </div>
            <div class="candidate-navigation">
                <a href="#" class="btn">Zurück</a>
                <a href="#" class="btn">Weiter</a>
            </div>
        </div>
    </div>
</div>
