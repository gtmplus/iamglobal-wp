<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header(); ?>
    <div class="wrap">
        <div class="single-wrap aside-wrap">
            <div class="article">
                <div class="content">
                    <div class="article-section">

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/post/content', get_post_format() );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>
                    </div>
                </div>
                <aside class="aside-section">
                    <div class="single-aside">
                        <?php iam_share_links() ?>
                        <div class="tags"><?php iam_entry_tags() ?></div>
                    </div>

                </aside>
            </div>


            <div class="blog-wrap posts-navigation">

	            <?php
	            $next_post = get_next_post();
	            if ( ! empty( $next_post ) ) : ?>
                    <article <?php post_class( 'post-next', $next_post->ID ); ?>>
                        <div class="content">
                            <div class="article-wrap">
                                <div class="entry-header">
						            <?php
						            echo sprintf( '<h2 class="entry-title"><a href="%1$s" rel="bookmark">%2$s</a></h2>',
							            get_permalink( $next_post ),
							            esc_html( $next_post->post_title )
						            );
						            ?>
                                </div>
                                <div class="entry-meta">
						            <?php
						            iam_entry_tags( $next_post->ID );
						            ?>
                                </div>
                            </div>
                        </div>
                    </article>
	            <?php endif; ?>

				<?php
				$prev_post = get_previous_post();
				if ( ! empty( $prev_post ) ) : ?>
                    <article <?php post_class( 'post-prev', $prev_post->ID ); ?>>
                        <div class="content">
                            <div class="article-wrap">
                                <div class="entry-header">
									<?php
									echo sprintf( '<h2 class="entry-title"><a href="%1$s" rel="bookmark">%2$s</a></h2>',
										get_permalink( $prev_post ),
										esc_html( $prev_post->post_title )
									);
									?>
                                </div>
                                <div class="entry-meta">
									<?php
									iam_entry_tags( $prev_post->ID );
									?>
                                </div>
                            </div>
                        </div>
                    </article>
				<?php endif; ?>

            </div>


        </div>
    </div><!-- .wrap -->
<?php get_footer();