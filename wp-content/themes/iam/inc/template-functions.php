<?php
/**
 * Additional features to allow styling of the templates
 */


add_action( 'init', 'register_post_types' );
function register_post_types(){
    register_post_type( 'employee', [
        'label'  => null,
        'labels' => [
            'name'               => 'Employee', // основное название для типа записи
            'singular_name'      => 'Employee', // название для одной записи этого типа
            'add_new'            => 'Add Employee', // для добавления новой записи
            'add_new_item'       => 'Add Employee', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Edit Employee', // для редактирования типа записи
            'new_item'           => 'New Employee', // текст новой записи
            'view_item'          => 'View Employee', // для просмотра записи этого типа.
            'search_items'       => 'Search Employee', // для поиска по этим типам записи
            'menu_name'          => 'Employee', // название меню
        ],
        'supports'            => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
        'taxonomies'          => [ 'employee_cat' ],
        'public'              => true,
        'menu_icon'           => 'dashicons-groups',
    ] );

    register_taxonomy( 'employee_cat', [ 'employee' ], [
        'labels'                => [
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'view_item '        => 'View Category',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        ],
        'meta_box_cb'           => 'post_categories_meta_box',
    ] );

    register_post_type( 'employer', [
        'label'  => null,
        'labels' => [
            'name'               => 'Employers', // основное название для типа записи
            'singular_name'      => 'Employer', // название для одной записи этого типа
            'add_new'            => 'Add Employer', // для добавления новой записи
            'add_new_item'       => 'Add Employer', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Edit Employer', // для редактирования типа записи
            'new_item'           => 'New Employer', // текст новой записи
            'view_item'          => 'View Employer', // для просмотра записи этого типа.
            'search_items'       => 'Search Employer', // для поиска по этим типам записи
            'menu_name'          => 'Employers', // название меню
        ],
        'supports'            => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
        'taxonomies'          => [ 'employer_cat' ],
        'public'              => true,
        'menu_icon'           => 'dashicons-groups',
    ] );

    register_taxonomy( 'employer_cat', [ 'employer' ], [
        'labels'                => [
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'view_item '        => 'View Category',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        ],
        'meta_box_cb'           => 'post_categories_meta_box',
    ] );
}

