<?php

/**
 * Manage WordPress styles and scripts.
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

add_action( 'init', function() {

    // Remove the REST API endpoint.
    remove_action('rest_api_init', 'wp_oembed_register_route');

    // Turn off oEmbed auto discovery.
    // Don't filter oEmbed results.
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

    // Remove oEmbed discovery links.
    remove_action('wp_head', 'wp_oembed_add_discovery_links');

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action('wp_head', 'wp_oembed_add_host_js');

	remove_action('wp_head', 'wp_generator');

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
	remove_action('wp_head', 'start_post_rel_link');
	remove_action('wp_head', 'adjacent_post_rel_link_wp_head');

	add_filter( 'feed_links_show_comments_feed', '__return_false' );

}, 500 );




add_filter( 'rest_authentication_errors', function( $result ) {

	// maybe authentication error already set
	if ( empty( $result ) ){
		if ( ! is_user_logged_in() )
			return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
	}

	return $result;
});


add_filter( 'script_loader_tag', 'add_attribs_to_scripts', 10, 3 );
function add_attribs_to_scripts( $tag, $handle, $src ) {

// The handles of the enqueued scripts we want to defer
    $async_scripts = array(
        'jquery-migrate'
    );

    $defer_scripts = array(
        'contact-form-7',
        'slick-js',
        'main-js',
    );


    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }
    if ( in_array( $handle, $async_scripts ) ) {
        return '<script src="' . $src . '" async="async" type="text/javascript"></script>' . "\n";
    }
    return $tag;
}



remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Disable W3TC footer comment for everyone but Admins (single site) / Super Admins (network mode)
if ( !current_user_can( 'unfiltered_html' ) ) {
	add_filter( 'w3tc_can_print_comment', function( $w3tc_setting ) { return false; }, 10, 1 );
}

add_action('wp_head',function() { ob_start(function($o) {
	return preg_replace('/\n?<.*?yoast seo plugin.*?>/mi','',$o);
}); },~PHP_INT_MAX);