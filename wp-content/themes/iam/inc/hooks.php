<?php

add_action( 'wp_ajax_nopriv_get_candidate_list', 'get_candidate_list' );
add_action( 'wp_ajax_rivo_get_candidate_list', 'get_candidate_list' );

function get_candidate_list($post_type = 'employee', $category = 'employee_cat', $category_id = null) {
	$ppp    = ( isset( $_POST['ppp'] ) ) ? $_POST['ppp'] : 3;
	$offset = ( isset( $_POST['offset'] ) ) ? $_POST['offset'] : 0;
	$post_type = ( isset( $_POST['post_type'] ) ) ? $_POST['post_type'] : $post_type;

	$args = array(
		'post_type'      => $post_type,
		'posts_per_page' => $ppp,
		'offset'         => $offset,
		'post_status'    => 'publish',
	);

	if( (isset( $_POST['category_id'] ) || $category_id !== null) && (isset( $_POST['category'] ) || $category !== null) ) {
        $category_id = ( isset( $_POST['category'] ) ) ? $_POST['category'] : $category_id;
        $category = ( isset( $_POST['category'] ) ) ? $_POST['category'] : $category;

        $args['tax_query'] = array(
            array(
                'taxonomy' => $category,
                'field'    => 'id',
                'terms'    => $category_id
            )
        );
    }

	$loop = new WP_Query( $args );

	if ( $loop->have_posts() ) :

		while ( $loop->have_posts() ) :
			$loop->the_post();

            get_template_part( 'template-parts/single', 'candidate' );

		endwhile;

	endif;

	wp_reset_postdata();
	//wp_die();
}

add_action( 'wp_ajax_nopriv_get_blog_list', 'get_blog_list' );
add_action( 'wp_ajax_rivo_get_blog_list', 'get_blog_list' );

function get_blog_list() {
    $ppp    = ( isset( $_POST['ppp'] ) ) ? $_POST['ppp'] : 4;
    $offset = ( isset( $_POST['offset'] ) ) ? $_POST['offset'] : 0;

    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => $ppp,
        'offset'         => $offset,
        'post_status'    => 'publish',
    );

//    if( (isset( $_POST['category_id'] ) || $category_id !== null) && (isset( $_POST['category'] ) || $category !== null) ) {
//        $category_id = ( isset( $_POST['category'] ) ) ? $_POST['category'] : $category_id;
//        $category = ( isset( $_POST['category'] ) ) ? $_POST['category'] : $category;
//
//        $args['tax_query'] = array(
//            array(
//                'taxonomy' => $category,
//                'field'    => 'id',
//                'terms'    => $category_id
//            )
//        );
//    }

    $loop = new WP_Query( $args );

    if ( $loop->have_posts() ) :

        while ( $loop->have_posts() ) :
            $loop->the_post();

            get_template_part( 'template-parts/loop', 'blog' );

        endwhile;

    endif;

    wp_reset_postdata();
    //wp_die();
}