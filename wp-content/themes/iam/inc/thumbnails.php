<?php
/**
 * Thumbnails configuration.
 *
 * @package Intellias
 */
add_action('after_setup_theme', 'iam_register_image_sizes', 5);

function iam_register_image_sizes() {
	add_image_size('candidate-thumb', 200, 200, array('center','center'));
	add_image_size('candidate-full', 685, 490, array('center','center'));
	add_image_size('blog-thumb', 350, 250, array('center','center'));
}
