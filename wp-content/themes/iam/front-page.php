<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<section id="first-screen" class="section first-screen">
    <div class="section-wrapper">
        <div class="left-screen" style="background-image: url(<?php echo get_field('left_image'); ?>);">
            <div class="screen-overlay"></div>
            <div class="screen-inner">
                <h3><a href="<?php echo site_url() . '/sections-page/'; ?>"><?php echo get_field('left_title'); ?></a></h3>
                <div class="hr"></div>
            </div>
        </div>
        <div class="right-screen" style="background-image: url(<?php echo get_field('right_image'); ?>">
            <div class="screen-overlay"></div>
            <div class="screen-inner">
                <h3><a href="<?php echo site_url() . '/sections-page-2/'; ?>"><?php echo get_field('right_title'); ?></a></h3>
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>