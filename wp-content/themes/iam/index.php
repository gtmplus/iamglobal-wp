<?php
/**
 * The main template file
 *
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<div class="wrap">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<div class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</div>
	<?php else : ?>
	<div class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'iam' ); ?></h2>
	</div>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' =>  '<span class="screen-reader-text">' . __( 'Previous page', 'iam' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'iam' ) . '</span>' ,
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'iam' ) . ' </span>',
				) );

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->
<?php get_footer();